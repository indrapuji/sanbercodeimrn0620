// Soal No. 1 (Range)
function range(startNum, finishNum) {
  var result = [];
  if (!startNum || !finishNum) {
    return -1;
  } else if (finishNum < startNum) {
    for (let i = startNum; i >= finishNum; i--) {
      result.push(i);
    }
  } else {
    for (let i = startNum; i <= finishNum; i++) {
      result.push(i);
    }
  }
  return result;
}
console.log(`====== NO 1 ======`);
console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

// Soal No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
  var result = [];
  if (startNum < finishNum) {
    for (let i = startNum; i <= finishNum; i += step) {
      result.push(i);
    }
  } else {
    for (let i = startNum; i >= finishNum; i -= step) {
      result.push(i);
    }
  }
  return result;
}
console.log(`====== NO 2 ======`);
console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

// Soal No. 3 (Sum of Range)
function sum(first, last, step) {
  var result = 0;
  if (!first && !last && !step) {
    return 0;
  }
  if (!last && !step) {
    return first;
  }
  if (!step) {
    if (first < last) {
      for (let i = first; i <= last; i += 1) {
        result += i;
      }
    } else {
      for (let i = first; i >= last; i -= 1) {
        result += i;
      }
    }
  } else {
    if (first < last) {
      for (let i = first; i <= last; i += step) {
        result += i;
      }
    } else {
      for (let i = first; i >= last; i -= step) {
        result += i;
      }
    }
  }
  return result;
}
console.log(`====== NO 3 ======`);
console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

// Soal No. 4 (Array Multidimensi)
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];
function dataHandling(data) {
  var result = "";

  for (i = 0; i <= data.length - 1; i++) {
    result += `Nomor ID: ${data[i][0]}\nNama Lengkap: ${data[i][1]}\nTTL: ${data[i][2]} ${data[i][3]} \nHobi: ${data[i][4]}\n \n`;
  }
  return result;
}
console.log(`====== NO 4 ======`);
console.log(dataHandling(input));

// Soal No. 5 (Balik Kata)
function balikKata(nama) {
  var result = "";
  for (let i = nama.length - 1; i >= 0; i--) {
    result += nama[i];
  }
  return result;
}
console.log(`====== NO 5 ======`);
console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I

// Soal No. 6 (Metode Array)
function dataHandling2(data) {
  data.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
  data.splice(4, 1, "Pria", "SMA Internasional Metro");
  console.log(data);

  var date = data[3].split("/");
  var month = "";
  switch (date[1]) {
    case "01":
      month = "Januari";
      break;
    case "02":
      month = "Februari";
      break;
    case "03":
      month = "Maret";
      break;
    case "04":
      month = "April";
      break;
    case "05":
      month = "Mei";
      break;
    case "06":
      month = "Juni";
      break;
    case "07":
      month = "Juli";
      break;
    case "08":
      month = "Agustus";
      break;
    case "09":
      month = "September";
      break;
    case "10":
      month = "Oktober";
      break;
    case "11":
      month = "November";
      break;
    default:
      month = "Desember";
      break;
  }
  console.log(month);

  var temp;
  for (i = 0; i < date.length - 1; i++) {
    for (j = 0; j < date.length - 1 - i; j++) {
      if (Number(date[j]) < Number(date[j + 1])) {
        temp = date[j];
        date[j] = date[j + 1];
        date[j + 1] = temp;
      }
    }
  }
  console.log(date);

  var year = date.shift();
  date.push(year);
  var day = date.join("-");
  console.log(day);

  var name = data[1].substr(0, 15);
  console.log(name);
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

console.log(`====== NO 6 ======`);
dataHandling2(input);
