// Soal No. 1 (Array to Object)
function arrayToObject(arr) {
  let result = {};
  let age = null;
  let now = new Date();
  let thisYear = now.getFullYear();
  for (let i = 0; i < arr.length; i++) {
    result.firstname = arr[i][0];
    result.lastname = arr[i][1];
    result.gender = arr[i][2];
    if (arr[i][3] > 0) {
      result.age = thisYear - arr[i][3];
    } else {
      result.age = `Invalid Birth Year`;
    }
    // console.log(`${i + 1}. ${arr[i][0]} ${arr[i][1]}: ${result}`);
    console.log(`${i + 1}. ${arr[i][0]} ${arr[i][1]}: ${JSON.stringify(result)}`); // agar object yang di dalam terbaca
  }
}

// Driver Code
var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
  var result = {};
  var buy = [];
  var tokoX = [
    ["Sepatu Stacattu", 1500000],
    ["Baju Zoro", 500000],
    ["Baju H&N", 250000],
    ["Sweater Uniklooh", 175000],
    ["Casing Handphone", 50000],
  ];
  if (!memberId) {
    return `Mohon maaf, toko X hanya berlaku untuk member saja`;
  } else {
    result.memberId = memberId;
  }
  if (money < 50000) {
    return `Mohon maaf, uang tidak cukup`;
  } else {
    result.money = money;
  }
  for (let i = 0; i < tokoX.length; i++) {
    if (money > 0 && money >= tokoX[i][1]) {
      money -= tokoX[i][1];
      buy.push(tokoX[i][0]);
    }
  }
  result.listPurchased = buy;
  result.changeMoney = money;
  return result;
}

// TEST CASES
console.log(shoppingTime("1820RzKrnWn08", 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime("82Ku8Ma742", 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
  rute = ["A", "B", "C", "D", "E", "F"];
  let cost = 2000;
  let result = [];
  if (!arrPenumpang) {
    return arrPenumpang;
  }
  for (let i = 0; i < arrPenumpang.length; i++) {
    result[i] = {};
    for (let j = 0; j < rute.length; j++) {
      result[i].penumpang = arrPenumpang[i][0];
      result[i].naikDari = arrPenumpang[i][1];
      result[i].tujuan = arrPenumpang[i][2];
      if (result[i].naikDari === rute[j]) {
        var from = j;
      }
      if (result[i].tujuan === rute[j]) {
        var destination = j;
      }
      result[i].bayar = (destination - from) * cost;
    }
  }
  return result;
}

//TEST CASE
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
