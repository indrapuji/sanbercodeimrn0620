var nama = "Jane";
var peran = "Werewolf";

if (nama === "") {
  console.log("Nama harus diisi!");
} else if (nama.length > 0 && peran.length < 0) {
  console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
} else if (nama.length > 0 && peran === "Penyihir") {
  console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
  console.log(`Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
} else if (nama.length > 0 && peran === "Guard") {
  console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
  console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`);
} else if (nama.length > 0 && peran === "Werewolf") {
  console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
  console.log(`Halo Werewolf ${nama}, Kamu akan memakan mangsa setiap malam!`);
}

var hari = 25;
var bulan = 12;
var tahun = 1945;

if (hari < 1 || hari > 31) {
  console.log(`hari antara 1 - 31`);
} else if (bulan < 1 || bulan > 12) {
  console.log(`bulan antara 1 - 12`);
} else if (tahun < 1900 || tahun > 2200) {
  console.log(`tahun antara 1900 - 2200`);
} else {
  switch (bulan) {
    case 1:
      bulan = "January";
      break;
    case 2:
      bulan = "February";
      break;
    case 3:
      bulan = "Maret";
      break;
    case 4:
      bulan = "April";
      break;
    case 5:
      bulan = "Mei";
      break;
    case 6:
      bulan = "Juni";
      break;
    case 7:
      bulan = "Juli";
      break;
    case 8:
      bulan = "Agustus";
      break;
    case 9:
      bulan = "September";
      break;
    case 10:
      bulan = "Oktober";
      break;
    case 11:
      bulan = "November";
      break;
    case 12:
      bulan = "Desember";
      break;
    default:
      break;
  }
  console.log(`${hari} ${bulan} ${tahun}`);
}

