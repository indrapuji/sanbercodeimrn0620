import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { Entypo } from "@expo/vector-icons";

export default function Card(props) {
  return (
    <View style={styles.skillContainer}>
      <Icon name={props.skill.item.iconName} size={80} color="#003366"></Icon>
      <View>
        <Text
          style={{
            color: "#003366",
            fontWeight: "bold",
            fontSize: 24,
            marginTop: 10,
            marginLeft: 5,
          }}
        >
          {props.skill.item.skillName}
        </Text>
        <Text style={{ color: "#3EC6FF", fontWeight: "bold", fontSize: 16, marginLeft: 5 }}>
          {props.skill.item.categoryName}
        </Text>
        <Text
          style={{
            fontSize: 48,
            color: "white",
            alignSelf: "flex-end",
            fontWeight: "bold",
            marginBottom: 10,
          }}
        >
          {props.skill.item.percentageProgress}
        </Text>
      </View>
      <Entypo name="chevron-right" size={80} color="#003366" />
    </View>
  );
}

const styles = StyleSheet.create({
  skillContainer: {
    flexDirection: "row",
    backgroundColor: "#B4E9FF",
    elevation: 5,
    borderRadius: 8,
    justifyContent: "space-around",
    alignItems: "center",
    marginBottom: 10,
    paddingHorizontal: 5,
  },
});
