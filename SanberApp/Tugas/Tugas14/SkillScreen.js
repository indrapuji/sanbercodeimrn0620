import React from "react";
import Card from "./components/Card";
import { StyleSheet, Text, View, FlatList, Image } from "react-native";
import skillData from "./skillData.json";

export default function SkillScreen() {
  return (
    <View style={styles.container}>
      <View style={{ top: 20 }}>
        <Image
          source={require("./images/logo-sanber.png")}
          style={{ width: 187.5, height: 51, alignSelf: "flex-end" }}
        />
        <View style={styles.user}>
          <Image
            source={require("./images/pic.jpg")}
            style={{ width: 35, height: 35, borderRadius: 25 }}
          />
          <View style={{ paddingLeft: 10 }}>
            <Text style={{ fontSize: 12 }}>Hai,</Text>
            <Text style={{ fontSize: 16, color: "#003366" }}>Indra Puji Novirwan</Text>
          </View>
        </View>
        <Text
          style={{
            color: "#003366",
            borderBottomColor: "#3EC6FF",
            fontSize: 36,
            paddingTop: 10,
          }}
        >
          SKILL
        </Text>
        <View
          style={{
            width: 343,
            height: 4,
            alignItems: "center",
            backgroundColor: "#3EC6FF",
            marginTop: 5,
          }}
        />
      </View>
      <View style={styles.category}>
        <View style={styles.categoryList}>
          <Text style={{ fontSize: 12, color: "#003366" }}>Library / Framework</Text>
        </View>
        <View style={styles.categoryList}>
          <Text style={{ fontSize: 12, color: "#003366" }}>Bahasa Pemrograman</Text>
        </View>
        <View style={styles.categoryList}>
          <Text style={{ fontSize: 12, color: "#003366" }}>Teknologi</Text>
        </View>
      </View>
      <FlatList
        data={skillData.items}
        renderItem={(skill) => <Card skill={skill} />}
        keyExtractor={(item) => item.id}
        style={{ marginTop: 10 }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  user: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 10,
  },
  category: {
    flexDirection: "row",
    marginTop: 30,
  },
  categoryList: {
    backgroundColor: "#B4E9FF",
    marginHorizontal: 5,
    padding: 5,
    borderRadius: 10,
  },
});
