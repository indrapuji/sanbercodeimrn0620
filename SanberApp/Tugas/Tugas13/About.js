import React from "react";
import { MaterialIcons } from "@expo/vector-icons";
import { Entypo } from "@expo/vector-icons";
import { FontAwesome } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from "react-native";

export default function About() {
  return (
    <View style={styles.container}>
      <Text
        style={{ marginTop: 15, fontSize: 30, color: "#003366", marginTop: 0, fontWeight: "bold" }}
      >
        Tentang Saya
      </Text>
      <Image
        source={require("./images/pic.jpg")}
        style={{ width: 100, height: 100, borderRadius: 300, marginTop: 10 }}
      />
      <Text style={{ marginTop: 15, fontSize: 25, color: "#003366", fontWeight: "bold" }}>
        Indra Puji Novirwan
      </Text>
      <Text style={{ marginTop: 5, fontSize: 15, color: "#3EC6FF" }}>React Native Developer</Text>
      <View style={styles.box}>
        <Text style={{ borderBottomWidth: 1, marginLeft: 10, marginTop: 5 }}>Portofolio</Text>
        <View style={styles.listPorto}>
          <View style={styles.detilPorto}>
            <AntDesign name="github" size={34} color="#3EC6FF" />
            <Text style={{ marginTop: 5 }}>@indrapuji</Text>
          </View>
          <View style={styles.detilPorto}>
            <AntDesign name="gitlab" size={34} color="#3EC6FF" />
            <Text style={{ marginTop: 5 }}>@indrapuji</Text>
          </View>
        </View>
      </View>
      <View style={styles.contact}>
        <Text style={{ borderBottomWidth: 1, marginLeft: 10, marginTop: 5, marginBottom: 5 }}>
          Hubungi Saya
        </Text>
        <View style={styles.list}>
          <View style={styles.detil}>
            <FontAwesome name="facebook-square" size={34} color="#3EC6FF" />
            <Text style={{ marginLeft: 10 }}>indra.puji.novirwan</Text>
          </View>
          <View style={styles.detil}>
            <AntDesign name="instagram" size={34} color="#3EC6FF" />
            <Text style={{ marginLeft: 10 }}>@indrapuji</Text>
          </View>
          <View style={styles.detil}>
            <AntDesign name="twitter" size={34} color="#3EC6FF" />
            <Text style={{ marginLeft: 10 }}>@indra_pn</Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
  },
  box: {
    display: "flex",
    backgroundColor: "#EFEFEF",
    height: 100,
    width: 320,
    marginBottom: 10,
    borderRadius: 10,
    marginTop: 10,
  },
  contact: {
    display: "flex",
    backgroundColor: "#EFEFEF",
    height: 180,
    width: 320,
    borderRadius: 20,
  },
  list: {
    display: "flex",
    marginLeft: 80,
  },
  detil: {
    display: "flex",
    flexDirection: "row",
    marginVertical: 5,
    alignItems: "center",
  },
  detilPorto: {
    display: "flex",
    alignItems: "center",
    marginTop: 10,
    justifyContent: "center",
  },
  listPorto: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
});
