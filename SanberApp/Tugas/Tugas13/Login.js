import React, { useState } from "react";
import { MaterialIcons } from "@expo/vector-icons";
import { Entypo } from "@expo/vector-icons";
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from "react-native";

export default function Login() {
  return (
    <View style={styles.container}>
      <Image
        source={require("./images/logo-sanber.png")}
        style={{ width: 300, height: 100, marginTop: -100 }}
      />
      <Text style={{ marginTop: 15, fontSize: 25 }}>Login</Text>
      <View style={styles.inputText}>
        <MaterialIcons style={{ marginRight: 5 }} name="email" size={24} color="black" />
        <TextInput
          placeholder="Input your email"
          style={{ fontSize: 13, marginLeft: 10 }}
        ></TextInput>
      </View>
      <View style={styles.inputText}>
        <Entypo style={{ marginRight: 5 }} name="key" size={24} color="black" />
        <TextInput
          placeholder="Input your password"
          style={{ fontSize: 13, marginLeft: 10 }}
        ></TextInput>
      </View>
      <TouchableOpacity style={styles.button}>
        <Text
          style={{
            backgroundColor: "#3EC6FF",
            color: "white",
            fontWeight: "bold",
            paddingHorizontal: 25,
            paddingVertical: 5,
            fontSize: 15,
          }}
        >
          Masuk
        </Text>
      </TouchableOpacity>
      <View style={{ marginTop: 15 }}>
        <Text style={{ fontWeight: "bold", color: "#3EC6FF", fontSize: 15 }}>Atau</Text>
      </View>
      <TouchableOpacity style={styles.button}>
        <Text
          style={{
            backgroundColor: "#003366",
            color: "white",
            fontWeight: "bold",
            paddingHorizontal: 25,
            paddingVertical: 5,
            fontSize: 15,
          }}
        >
          Daftar ?
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
  },
  text: {
    fontSize: 30,
    color: "#04063c",
  },
  inputText: {
    marginTop: 20,
    width: 200,
    height: 30,
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderColor: "#003366",
    flexDirection: "row",
  },
  button: {
    marginTop: 20,
    height: 35,
    alignItems: "center",
    borderRadius: 50,
  },
});
