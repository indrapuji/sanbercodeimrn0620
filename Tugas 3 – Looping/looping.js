// No. 1 Looping While
var count = 2;
var number = 0;
console.log(`LOOPING PERTAMA`);
while (number < 20) {
  number += count;
  console.log(`${number} - I love coding`);
}
console.log(`LOOPING KEDUA`);
while (number >= count) {
  console.log(`${number} - I will become a mobile developer`);
  number -= count;
}

console.log(`===================================`)

// No. 2 Looping menggunakan for
for (let i = 1; i <= 20; i++) {
  if (i % 2 == 0) {
    console.log(`${i} - Berkualitas`);
  } else if (i % 3 == 0) {
    console.log(`${i} - I Love Coding  `);
  } else {
    console.log(`${i} - Santai`);
  }
}

console.log(`===================================`)

// No. 3 Membuat Persegi Panjang #
var hastag = ``;
var line = 4;
var column = 8;
for (let i = 0; i < line; i++) {
  for (let j = 0; j < column; j++) {
    hastag += `#`;
  }
  console.log(hastag);
  hastag = ``;
}

console.log(`===================================`)

// No. 4 Membuat Tangga
var asterix = `*`;
var linecount = 7;
for (let i = 0; i < linecount; i++) {
  console.log(asterix);
  asterix += `*`;
}

console.log(`===================================`)

// No. 5 Membuat Papan Catur
var board = 8;
var line = "";
for (let i = 0; i < board; i++) {
  if (i % 2 == 0) {
    for (let j = 0; j < board; j++) {
      if (j % 2 == 0) {
        line += " ";
      } else {
        line += "#";
      }
    }
  } else {
    for (let k = 0; k < board; k++) {
      if (k % 2 == 0) {
        line += "#";
      } else {
        line += " ";
      }
    }
  }
  console.log(line);
  line = "";
}
